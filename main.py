from flask import *
import json, time

api = Flask(__name__)


@api.route('/', methods=['GET'])
def Main_page():
    data_set = {
        'Page': 'Main',
        'Message': 'Sucessfully loaded the Main page',
        'Timestamp': time.time()
    }
    json_dump = json.dumps(data_set)

    return json_dump

if __name__ == '__main__':
    api.run(host='0.0.0.0', port=8080)
